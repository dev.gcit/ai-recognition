FROM python:3.8-slim
# RUN apt-get update -y
# RUN apt-get install -y software-properties-common
# RUN apt-add-repository universe
RUN apt-get update -y

# RUN apt-get install -y python-pip python-dev build-essential
RUN mkdir /app
COPY . /app
WORKDIR /app
RUN pip install -r libraries.txt
ADD . /app
EXPOSE 5000
ENV DEPLOY=config_dply.cfg
# CMD gunicorn --bind 0.0.0.0:5000 app:app
CMD gunicorn --bind 0.0.0.0:$PORT app:app
# RUN chmod +x ./entrypoint.sh
# ENTRYPOINT ["sh", "entrypoint.sh"]
# ENTRYPOINT ["./gunicorn_starter.sh"]
# ENTRYPOINT ["python"]
# CMD ["app.py"]
